<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->id_cat_status===1) {
            //Get list users and status
            $users = Users::select('users.id','users.name','users.email','cat_status.status')
            ->join('cat_status', 'cat_status.id', '=', 'users.id_cat_status')
            ->get();
            return view('home',['users' => $users]);
        } else {
            //Auth::logout();
            //Sentry::logout();
            session()->flush();
            return redirect('/login');
        }
    }
    /**
     * Edit a user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edituser(Request $request)
    {
        if(empty($request->input('password'))||empty($request->input('user'))) {
            $affected = false;
        } else {
            $values['email'] = $request->input('email');
            if(!empty($request->input('password'))){
                $values['password'] = Hash::make($request->input('password'));
            }
            $affected = Users::where('id', $request->input('user'))
            ->update($values);
        }
        return response()->json([
            'msg'       => $affected?'Success':'Try again!',
            'status'    => $affected?'success':'error',
            'data'      => ['user' => $request->input('user'), 'email' => $request->input('email'),]
        ]);
    }
    /**
     * Edit for user status.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edituserstatus(Request $request)
    {
        if(empty($request->input('user'))) {
            $affected = false;
        } else {
            //Get list users and status
            $user = Users::select('id_cat_status')
            ->where('id', '=', $request->input('user'))
            ->first();
            if(empty($user)){
                $affected = false;
            } else {
                $affected = Users::where('id', $request->input('user'))
                ->update(['id_cat_status' => ($user->id_cat_status==1?2:1)]);
            }
        }
        return response()->json([
            'msg'       => $affected?'Success':'Try again!',
            'status'    => $affected?'success':'error'
        ]);
    }
}
