@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('List users') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col" width="20%">Status</th>
                            <th scope="col">Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $key=>$row)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->email}}</td>
                            <td>
                              <!-- Input for change status -->
                              <div class="form-check form-switch">
                                <input class="form-check-input editUsuserStatus" type="checkbox" role="switch" id="flexSwitchCheckDefault_{{$row->id}}" user="{{$row->id}}" {{$row->status==='Active'?'checked':''}}>
                              </div>
                              <label class="form-check-label" for="flexSwitchCheckDefault_{{$row->id}}">{{$row->status}}</label>
                            </td>
                            <!-- Button for edit user -->
                            <td><button class="btn btn-primary editUser" user="{{$row->id}}"><i class="bi bi-pencil"></i></button></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!--{{ __('You are logged in!') }}-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal for edit user -->
<div class="modal fade" id="editUserModal" tabindex="-1" aria-labelledby="editUserModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editUserModalLabel">Editar</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="form-editUser" method="POST" action="edituser" class="was-validated">
        @csrf
          <fielset>
            <input type='hidden' id="user" name="user">
            <div class="mb-3">
            <label>Name</label>
            <div id="name" class="form-text"></div>
            </div>
            <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" required>
            <div id="emailHelp" class="form-text"></div>
            </div>
            <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" id="password" name="password">
            </div>
          </fielset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" form="form-editUser">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const Toast = Swal.mixin({
      toast: true,
      position: 'top-center',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    });
  $(document).ready(function () {
    //Event of button editUser
    $('.editUser').on('click',function(){
      $('#form-editUser input:not([name="_token"])').val('');
      var tr = $(this).parent().parent();
      $('#user').val($(this).attr('user'));
      $('#name').text(tr.find('td:eq(1)').text());
      $('#email').val(tr.find('td:eq(2)').text());
      $("#editUserModal").modal('show');
    });
    //Submit for edit user
    $('#form-editUser').on('submit',function(event){
      event.preventDefault();
      var form = $(this);
      var data = form.serialize()
      form.parent().parent().find('button').attr({disabled:true});
      // Assign handlers immediately after making the request,
      // and remember the jqXHR object for this request
      var jqxhr = $.ajax({
          method: form.attr('method'),
          url: form.attr('action'),
          data: data,
          dataType: "json"
        })
        .done(function(data) {
          if(data.status==='success'){
            var tr = $('button[user="'+data.data.user+'"]').parent().parent();
            tr.find('td:eq(2)').text(data.data.email);
          }
          Toast.fire({
            icon: data.status,
            title: data.msg
          });
        })
        .fail(function() {
          Toast.fire({
            icon: 'error',
            title: '¡Vuelva a intentar!'
          });
        })
        .always(function() {
          form.parent().parent().find('button').attr({disabled:false});
        });
      // Perform other work here ...
      // Set another completion function for the request above
      jqxhr.always(function() {});
    });

    $('.editUsuserStatus').on('click',function() {
      var input = $(this);
      input.attr({disabled:true});
      // Assign handlers immediately after making the request,
      // and remember the jqXHR object for this request
      var jqxhr = $.ajax({
          method: 'POST',
          url: 'edituserstatus',
          data: {_token:$('[name="_token"]').val(),user:$(this).attr('user')},
          dataType: "json"
        })
        .done(function(data) {
          if(data.status!=='success'){
            input.attr({checked:!input.is(':checked')});
          } else {
            input.parent().parent().find('label').text(input.parent().parent().find('label').text()=='Active'?'Suspended':'Active')
          }
          Toast.fire({
            icon: data.status,
            title: data.msg
          });
        })
        .fail(function() {
          Toast.fire({
            icon: 'error',
            title: '¡Vuelva a intentar!'
          });
        })
        .always(function() {
          input.attr({disabled:false});
        });
      // Perform other work here ...
      // Set another completion function for the request above
      jqxhr.always(function() {});
      //$(this).attr({checked:$(this).is(':checked')});
    });
  });
</script>
 @endsection
